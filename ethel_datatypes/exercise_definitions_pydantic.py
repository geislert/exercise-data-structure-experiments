from pydantic import BaseModel, Field, validator
from typing import List, Literal
from uuid import UUID


class OptionAnswer(BaseModel):
    uuid: UUID = Field(..., description="Unique identifier for the option")
    text: str = Field(..., description="The text of the option")
    is_correct: bool = Field(..., description="Whether this option is the correct answer")

    @validator("text")
    def text_not_empty(cls, value):
        if not value.strip():
            raise ValueError("Option text cannot be empty")
        return value


class NumericalAnswer(BaseModel):
    value: float = Field(..., description="The correct numerical answer")
    unit: str = Field(None, description="The unit of the answer (e.g., 'cm', 'kg')")
    tolerance: float = Field(..., description="Acceptable absolute tolerance for the answer") # absolute tolerance - if relative, the corresponding absolute value should be calculated by the system before making the exercise available

    @validator("tolerance")
    def tolerance_must_be_positive(cls, value):
        if value < 0:
            raise ValueError("Tolerance must be a non-negative number")
        return value


class ExerciseBase(BaseModel):
    uuid: UUID = Field(..., description="Unique identifier for the exercise")
    text: str = Field(..., description="The exercise text")
    answer_type: Literal["multiple_choice", "numerical", "radio_button"] = Field(
        ..., description="The type of exercise"
    )

    @validator("text")
    def text_not_empty(cls, value):
        if not value.strip():
            raise ValueError("Exercise text cannot be empty")
        return value


class MultipleChoiceExercise(ExerciseBase):
    answer_type: Literal["multiple_choice"] = "multiple_choice"
    options: List[OptionAnswer] = Field(..., description="List of available options for the exercise")

    @validator("options")
    def validate_options(cls, options):
        if len(options) < 2:
            raise ValueError("A multiple-choice exercise must have at least two options")
        return options


class RadioButtonExercise(MultipleChoiceExercise):
    answer_type: Literal["radio_button"] = "radio_button"

    @validator("options")
    def validate_radio_button_options(cls, options):
        if sum(option.is_correct for option in options) != 1:
            raise ValueError("A radio button exercise must have exactly one correct answer")
        return options


class NumericalExercise(ExerciseBase):
    answer_type: Literal["numerical"] = "numerical"
    answer: NumericalAnswer = Field(..., description="Details about the correct numerical answer")
