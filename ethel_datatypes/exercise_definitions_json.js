const optionAnswerSchema = {
  $id: "https://example.com/schemas/option-answer.json",
  title: "OptionAnswer",
  type: "object",
  properties: {
    uuid: {
      type: "string",
      format: "uuid",
      description: "Unique identifier for the option"
    },
    text: {
      type: "string",
      description: "Text of the option"
    },
    is_correct: {
      type: "boolean",
      description: "Indicates if the option is correct"
    }
  },
  required: ["uuid", "text", "is_correct"],
  additionalProperties: false
};

const numericalAnswerSchema = {
  $id: "https://example.com/schemas/numerical-answer.json",
  title: "NumericalAnswer",
  type: "object",
  properties: {
    value: {
      type: "number",
      description: "The correct numerical answer"
    },
    unit: {
      type: ["string", "null"],
      description: "Unit of the answer (e.g., 'cm', 'kg')"
    },
    tolerance: {
      type: "number",
      minimum: 0,
      description: "Acceptable absolute tolerance for the answer"
    }
  },
  required: ["value", "tolerance"],
  additionalProperties: false
};

const exerciseBaseSchema = {
  $id: "https://example.com/schemas/exercise-base.json",
  title: "ExerciseBase",
  type: "object",
  properties: {
    uuid: {
      type: "string",
      format: "uuid",
      description: "Unique identifier for the exercise"
    },
    text: {
      type: "string",
      description: "The exercise text"
    },
    answer_type: {
      type: "string",
      enum: ["multiple_choice", "numerical", "radio_button"],
      description: "The type of exercise"
    }
  },
  required: ["uuid", "text", "answer_type"],
  additionalProperties: false
};

const multipleChoiceExerciseSchema = {
  $id: "https://example.com/schemas/multiple-choice-exercise.json",
  title: "MultipleChoiceExercise",
  type: "object",
  allOf: [
    { $ref: "https://example.com/schemas/exercise-base.json" }
  ],
  properties: {
    answer_type: {
      const: "multiple_choice"
    },
    options: {
      type: "array",
      description: "List of options for the exercise",
      items: { $ref: "https://example.com/schemas/option-answer.json" },
      minItems: 2
    }
  },
  required: ["options"],
  additionalProperties: false
};

const radioButtonExerciseSchema = {
  $id: "https://example.com/schemas/radio-button-exercise.json",
  title: "RadioButtonExercise",
  type: "object",
  allOf: [
    { $ref: "https://example.com/schemas/multiple-choice-exercise.json" }
  ],
  properties: {
    answer_type: {
      const: "radio_button"
    }
  },
  allOf: [
    {
      properties: {
        options: {
          items: {
            oneOf: [
              {
                properties: {
                  is_correct: { const: true }
                }
              },
              {
                properties: {
                  is_correct: { const: false }
                }
              }
            ]
          }
        }
      }
    }
  ]
};

const numericalExerciseSchema = {
  $id: "https://example.com/schemas/numerical-exercise.json",
  title: "NumericalExercise",
  type: "object",
  allOf: [
    { $ref: "https://example.com/schemas/exercise-base.json" }
  ],
  properties: {
    answer_type: {
      const: "numerical"
    },
    answer: {
      $ref: "https://example.com/schemas/numerical-answer.json"
    }
  },
  required: ["answer"],
  additionalProperties: false
};

// Export schemas as a module for reuse
module.exports = {
  optionAnswerSchema,
  numericalAnswerSchema,
  exerciseBaseSchema,
  multipleChoiceExerciseSchema,
  radioButtonExerciseSchema,
  numericalExerciseSchema
};
